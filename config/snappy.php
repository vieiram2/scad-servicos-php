<?php

if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
    $pdfBinary   = base_path('vendor\bin\wkhtmltopdf.exe');
    $imageBinary = base_path('vendor\bin\wkhtmltoimage.exe');
} else {
    $pdfBinary   = base_path('vendor/bin/wkhtmltopdf-amd64');
    $imageBinary = base_path('vendor/bin/wkhtmltoimage-amd64');
}

$options = array();

$httpProxy = env('HTTP_PROXY', env('http_proxy'));
if (!empty($httpProxy)) {
    $options['proxy'] = $httpProxy;
}

return array(


    'pdf' => array(
        'enabled' => true,
        'binary'  => env('WKHTMLTOPDF_BINARY') ?: $pdfBinary,
        'timeout' => false,
        'options' => $options,
        'env'     => array(),
    ),
    'image' => array(
        'enabled' => true,
        'binary'  => env('WKHTMLTOIMAGE_BINARY') ?: $imageBinary,
        'timeout' => false,
        'options' => $options,
        'env'     => array(),
    ),


);
