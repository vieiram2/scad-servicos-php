<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Atividade extends Model
{
    /**
     * @var string
     */
    protected $table = 'atividade';

    /**
     * @var array
     */
    protected $fillable = [
        'titulo',
        'data_inicio',
        'data_fim',
        'qtd_alunos',
        'tipo_atividade_id',
        'qtd_horas',
        'qtd_capitulos',
        'documento',
        'relatorio_id',
        'pontuacao_total',
        'pontuacao_corrigida',
    ];

    /**
     * @var array
     */
    protected $hidden = [
        'documento',
    ];

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tipo()
    {
        return $this->belongsTo(TipoAtividade::class, 'tipo_atividade_id');
    }

    public function relatorio()
    {
        return $this->belongsTo(Relatorio::class, 'relatorio_id');
    }
}
