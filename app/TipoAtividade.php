<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoAtividade extends Model
{
    /**
     * @var string
     */
    protected $table = 'tipo_atividade';

    /**
     * @var array
     */
    protected $fillable = [
        'nome',
        'descricao',
        'qtd_ponto',
        'unidade',
        'qtd_minima',
        'teto',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function atividades()
    {
        return $this->hasMany(Atividade::class, 'atividade_id');
    }
}
