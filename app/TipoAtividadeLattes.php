<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoAtividadeLattes extends Model
{
    /**
     * @var string
     */
    protected $table = 'tipo_atividade_lattes';

    /**
     * @var array
     */
    protected $fillable = [
        'tipo_atividade_id',
        'tipo_producao',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tiposAtividades()
    {
        return $this->hasMany(TipoAtividade::class, 'tipo_atividade_id');
    }
}
