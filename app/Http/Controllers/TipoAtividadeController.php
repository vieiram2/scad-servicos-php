<?php

namespace App\Http\Controllers;

use App\TipoAtividade;
use Illuminate\Http\Request;

class TipoAtividadeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $tiposAtividades = TipoAtividade::all();

        return response()->json($tiposAtividades);
    }
}
