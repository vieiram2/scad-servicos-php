<?php

namespace App\Http\Controllers;

use App\Atividade;
use App\TipoAtividadeLattes;
use Illuminate\Http\Request;
use SimpleXMLElement;

class ImportadorLattesController extends Controller
{
    /** @var array */
    private $tipoAtividadeLattesCache;

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /** @var \Illuminate\Http\UploadedFile $curriculo */
            $curriculo = $request->file('curriculo');

        if (empty($curriculo) || !$curriculo->isValid()) {
            return response()->json(['success' => false]);
        }

        $xml = simplexml_load_file($curriculo->getRealPath());
        // $xml = new SimpleXMLElement($curriculo->getRealPath(), 0, true);

        $this->tipoAtividadeLattesCache = TipoAtividadeLattes::all()->keyBy('tipo_producao')->toArray();

        $parsed = $this->parse($xml);

        $save = true;
        if ($save) {
            foreach ($parsed as $atividade) {
                if (!empty($atividade['tipo_atividade_id'])) {
                    Atividade::create($atividade);
                }
            }
        }

        return response()->json($parsed);
    }

    /**
     * @param SimpleXMLElement $curriculoVitae
     *
     * @return array
     * @fixme Adicionar um metadado / atributos na tabela para poder identificar atividade dentro de um mesmo nó
     */
    private function parse($curriculoVitae)
    {
        $atividades = [];

        foreach ($curriculoVitae->{'PRODUCAO-BIBLIOGRAFICA'} as $producoesBibliograficas) {
             $atividades = array_merge($atividades, $this->parseProducoesBibliograficas($producoesBibliograficas));
        }

        foreach ($curriculoVitae->{'PRODUCAO-TECNICA'} as $producoesTecnicas) {
             $atividades = array_merge($atividades, $this->parseProducoesTecnicas($producoesTecnicas));
        }

        foreach ($curriculoVitae->{'OUTRA-PRODUCAO'} as $outrasProducoes) {
            $atividades = array_merge($atividades, $this->parseOutrasProducoes($outrasProducoes));
        }

        return $atividades;
    }

    private function parseProducoesBibliograficas($producoesBibliograficas)
    {
        $atividades = [];

        foreach ($producoesBibliograficas->{'TRABALHOS-EM-EVENTOS'} as $trabalhosEmEventos) {
            foreach ($trabalhosEmEventos->{'TRABALHO-EM-EVENTOS'} as $trabalhoEmEvento) {
                $atividades[] = $this->parseTrabalhoEmEvento($trabalhoEmEvento);
            }
        }

        foreach ($producoesBibliograficas->{'ARTIGOS-PUBLICADOS'} as $artigosPublicados) {
            foreach ($artigosPublicados->{'ARTIGO-PUBLICADO'} as $artigoPublicado) {
                $atividades[] = $this->parseArtigoPublicado($artigoPublicado);
            }
        }

        return $atividades;
    }

    private function parseProducoesTecnicas($producoesTecnicas) {
        $atividades = [];

        foreach ($producoesTecnicas->{'SOFTWARE'} as $software) {
            $atividades[] = $this->parseSoftware($software);
        }

        foreach ($producoesTecnicas->{'TRABALHO-TECNICO'} as $trabalhoTecnico) {
            $atividades[] = $this->parseTrabalhoTecnico($trabalhoTecnico);
        }

        return $atividades;
    }

    private function parseOutrasProducoes($outrasProducoes) {
        $atividades = [];

        foreach ($outrasProducoes->{'ORIENTACOES-CONCLUIDAS'} as $orientacoesConcluidas) {
            foreach ($orientacoesConcluidas->{'ORIENTACOES-CONCLUIDAS-PARA-MESTRADO'} as $orientacaoConcluidaParaMestrado) {
                 $atividades[] = $this->parseOrientacaoConcluidaParaMestrado($orientacaoConcluidaParaMestrado);
            }

            foreach ($orientacoesConcluidas->{'OUTRAS-ORIENTACOES-CONCLUIDAS'} as $outraOrientacaoConcluida) {
                $atividades[] = $this->parseOutraOrientacaoConcluida($outraOrientacaoConcluida);
            }
        }

        return $atividades;
    }

    private function parseTrabalhoEmEvento($trabalhoEmEvento)
    {
        $dadosBasicos = $trabalhoEmEvento->{'DADOS-BASICOS-DO-TRABALHO'}->attributes();

        return [
            'method' => __METHOD__,

            'titulo' => (string) $dadosBasicos['TITULO-DO-TRABALHO'],
//            'data_inicio' => null,
//            'data_fim' => null,
//            'qtd_alunos' => 0,
            'tipo_atividade_id' => '',
//            'qtd_horas' => null,
//            'qtd_capitulos' => null,
//            'documento' => null,
//            'relatorio_id' => null,
//            'pontuacao_total' => null,
//            'pontuacao_corrigida' => null,
        ];
    }

    private function parseArtigoPublicado($artigoPublicado)
    {
        $tipoAtividadeLattes = $this->tipoAtividadeLattesCache['ARTIGOS-PUBLICADOS'];

        $dadosBasicos = $artigoPublicado->{'DADOS-BASICOS-DO-ARTIGO'}->attributes();

        return [
            'method' => __METHOD__,

            'titulo' => (string) $dadosBasicos['TITULO-DO-ARTIGO'],
            'tipo_atividade_id' => (string) $tipoAtividadeLattes['tipo_atividade_id'],
        ];
    }

    private function parseSoftware($software)
    {
        $dadosBasicos = $software->{'DADOS-BASICOS-DO-SOFTWARE'}->attributes();

        return [
            'method' => __METHOD__,

            'titulo' => (string) $dadosBasicos['TITULO-DO-SOFTWARE'],
            'tipo_atividade_id' => '',
        ];
    }

    private function parseTrabalhoTecnico($trabalhoTecnico)
    {
        $dadosBasicos = $trabalhoTecnico->{'DADOS-BASICOS-DO-TRABALHO-TECNICO'}->attributes();

        return [
            'method' => __METHOD__,

            'titulo' => (string) $dadosBasicos['TITULO-DO-TRABALHO-TECNICO'],
            'tipo_atividade_id' => '',
        ];
    }

    private function parseOrientacaoConcluidaParaMestrado($orientacaoConcluidaParaMestrado)
    {
        $tipoAtividadeLattes = $this->tipoAtividadeLattesCache['ORIENTACOES-CONCLUIDAS-PARA-MESTRADO'];

        $dadosBasicos = $orientacaoConcluidaParaMestrado->{'DADOS-BASICOS-DE-ORIENTACOES-CONCLUIDAS-PARA-MESTRADO'}->attributes();

        return [
            'method' => __METHOD__,

            'titulo' => (string) $dadosBasicos['TITULO'],
            'tipo_atividade_id' => (string) $tipoAtividadeLattes['tipo_atividade_id'],
        ];
    }

    private function parseOutraOrientacaoConcluida($outraOrientacaoConcluida)
    {
        $dadosBasicos = $outraOrientacaoConcluida->{'DADOS-BASICOS-DE-OUTRAS-ORIENTACOES-CONCLUIDAS'}->attributes();

        return [
            'method' => __METHOD__,

            'titulo' => (string) $dadosBasicos['TITULO'],
            // FIXME "Outras orientações" estão sem tipo no xml de exemplo
            'tipo_atividade_id' => '',
        ];
    }
}
