<?php

namespace App\Http\Controllers;

use App;
use App\Atividade;
use App\Relatorio;
use Illuminate\Http\Request;
use Twig_Environment;
use Twig_Loader_Filesystem;

class RelatorioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $atividades = Relatorio::all();

        return response()->json($atividades);
    }

    /**
     * @param $id
     *
     * @return \Symfony\Component\HttpFoundation\StreamedResponse
     */
    public function pdf($id)
    {
        /** @noinspection PhpUnhandledExceptionInspection */

        $relatorio = Relatorio::with(['professor'])
            ->where(['id' => $id])
            ->firstOrFail();
        $atividades = Atividade::with(['tipo'])
            ->where(['relatorio_id' => $id])
            ->get();

        $listaAtividades = $this->criarListaAtividades($relatorio->toArray(), $atividades->toArray());

        $comprovantes = $this->getFiles();

        $pdf = $this->mergePdfs(array_merge([$listaAtividades], $comprovantes));

        $contents = file_get_contents($pdf);

        @unlink($listaAtividades);
        @unlink($pdf);

        $callback = function () use ($contents) {
            echo $contents;
        };

        $headers = [
            'Content-Type'        => 'application/pdf',
//            'Content-Disposition' => 'attachment; filename="file.pdf"',
        ];

        return response()->stream($callback, 200, $headers);
    }

    /**
     * @param array $relatorio
     * @param array $atividades
     *
     * @return string
     */
    private function criarListaAtividades($relatorio, $atividades)
    {
        $loader = new Twig_Loader_Filesystem(resource_path('templates'));
        $twig = new Twig_Environment($loader);

        $template = $twig->load('relatorio.html.twig');

        $html = $template->render([
            'relatorio'  => $relatorio,
            'atividades' => $atividades,
        ]);

        /** @var \Knp\Snappy\Pdf $snappy */
        $snappy = App::make('snappy.pdf');

        $snappy->setOption('encoding', 'UTF-8');
        $snappy->setOption('enable-javascript', true);
        $snappy->setOption('javascript-delay', 1500);
        $snappy->setOption('enable-smart-shrinking', true);
        $snappy->setOption('no-stop-slow-scripts', true);
        $snappy->setOption('orientation', 'landscape');

        $outputPath = tempnam(sys_get_temp_dir(), 'listaAtividades_');
        @unlink($outputPath);
        $outputPath .= ".pdf";

        $snappy->generateFromHtml($html, $outputPath);

        return $outputPath;
    }

    /**
     * @param array $pdfs
     *
     * @return string
     */
    private function mergePdfs($pdfs)
    {
        $outputFile = tempnam(sys_get_temp_dir(), 'relatorio_');

        @unlink($outputFile);

        $outputFile .= ".pdf";

        $cmd = "\"" . $this->getGhostscriptPath() . "\" -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dNOPAUSE -dQUIET -dBATCH -sOutputFile=\"$outputFile\"";

        foreach ($pdfs as $pdf) {
            $cmd .= " \"$pdf\"";
        }

        exec($cmd, $output, $returnVar);

        if ($returnVar !== 0) {
            throw new \Exception(sprintf("Erro na execução do Ghostscript: (%d) '%s'.\nComando: '%s'", $returnVar, implode($output), $cmd));
        }

        return $outputFile;
    }

    /**
     * @return string[]
     */
    private function getFiles()
    {
        $path = trim(storage_path('app')) . DIRECTORY_SEPARATOR . 'relatorio';

        $filenames = array_diff(scandir($path), ['.', '..']);

        $files = array_map(function ($filename) use ($path) {
            return $path . DIRECTORY_SEPARATOR . $filename;
        }, $filenames);

        return array_values($files);
    }

    /**
     * @return string
     * @throws \Exception
     */
    private function getGhostscriptPath() {
        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            $binary = base_path('vendor\bin\gswin32c.exe');
        } else {
            $binary = base_path('vendor/bin/gs-923-linux-x86_64');
        }

        $binary = env('GHOSTSCRIPT_BINARY') ?: $binary;

        if (!file_exists($binary)) {
            throw new \Exception(sprintf("Binário do Ghostcript não encontrado! Caminho configurado: '%s'", $binary));
        }

        return $binary;
    }

}
