<?php

namespace App\Http\Controllers;

use App;
use Illuminate\Http\Request;

class Html2PdfController extends Controller
{
    /**
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->validate($request, [
            'url'        => 'required',
            'background' => 'nullable|in:0,1'
        ]);

        set_time_limit(60 * 3);

        /** @var \Knp\Snappy\Pdf $snappy */
        $snappy = App::make('snappy.pdf');

        $snappy->setOption('enable-javascript', true);
        $snappy->setOption('javascript-delay', 1500);
        $snappy->setOption('enable-smart-shrinking', true);
        $snappy->setOption('no-stop-slow-scripts', true);

        $noBackground = isset($request['background']) && !$request['background'];
        $snappy->setOption('no-background', $noBackground);

        try {
            $output = $snappy->getOutput($request['url']);
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'message' => 'Falha ao gerar PDF a partir do endereço \'' . $request['url'] . '\'']);
        }

        $callback = function () use ($output) {
            echo $output;
        };

        $headers = [
            'Content-Type'        => 'application/pdf',
//            'Content-Disposition' => 'attachment; filename="file.pdf"',
        ];

        return response()->stream($callback, 200, $headers);
    }
}
