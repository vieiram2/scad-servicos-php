<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Relatorio extends Model
{
    /**
     * @var string
     */
    protected $table = 'relatorio';

    /**
     * @var array
     */
    protected $fillable = [
        'data_inicio',
        'data_fim',
        'pontuacao_dia',
        'professor_siape',
    ];

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function professor()
    {
        return $this->belongsTo(Professor::class, 'professor_siape', 'siape');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function atividades()
    {
        return $this->hasMany(Atividade::class, 'relatorio_id');
    }
}
