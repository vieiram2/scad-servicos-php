<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Professor extends Model
{
    /**
     * @var string
     */
    protected $table = 'professor';

    /**
     * @var array
     */
    protected $fillable = [
        'siape',
        'afastado',
        'carga_atual',
        'cnome',
        'data_aposentadoria',
        'data_exoneracao',
        'data_ingresso',
        'data_nasc',
        'data_saida',
        'lotacao',
        'nome',
        'regime',
        'status',
        'senha',
        'administrativo',
    ];

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function relatorios()
    {
        return $this->hasMany(Relatorio::class, 'professor_siape', 'siape');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function atividades()
    {
        return $this->hasManyThrough(
            Atividade::class,
            Relatorio::class,
            'professor_siape',
            'relatorio_id',
            'siape',
            'professor_siape'
        );
    }
}
