<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
<form method="POST" action="/api/importador-lattes?XDEBUG_SESSION_START=PHPSTORM" enctype="multipart/form-data">
    {{ csrf_field() }}

    <label>
        curriculo.xml:
            <input type="file" id="curriculo" name="curriculo">
    </label>
    <input type="submit">
</form>
</body>
</html>
